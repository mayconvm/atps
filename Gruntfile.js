'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.
    clean: {
      src: ['dist']
    },
    copy: {
      main: {
        files: [
          // bootstrap
          {expand: true, src: ['bower_components/bootstrap/dist/js/*'], dest: 'dist/js', filter:'isFile', flatten: true,},
          {expand: true, src: ['bower_components/bootstrap/dist/css/*'], dest: 'dist/css', filter:'isFile', flatten: true,},
          {expand: true, src: ['bower_components/bootstrap/dist/fonts/*'], dest: 'dist/fonts', filter:'isFile', flatten: true,},
          
          // imagens
          {expand: true, src: ['img/*'], dest: 'dist/'},

          // jQuery
          {expand: true, src: ['bower_components/jquery/dist/*'], dest: 'dist/js', filter:'isFile', flatten: true,},
        ]
      }
    },
    concat: {
      options: {
        separator: ';',
      },
      basic_and_extras: {
        files: {
          'dist/css/kernel.min.css': ['src/css/*.css'],
          'dist/js/kernel.min.js': ['src/js/*.js'],
        },
      }
    },

    // uglify: {
    //   options: {
    //     banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
    //   },
    //   build: {
    //     src: 'bower_components/src/<%= pkg.name %>.js',
    //     dest: 'js/dist/<%= pkg.name %>.min.js'
    //   }
    // }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  // grunt.loadNpmTasks('grunt-contrib-qunit');
  // grunt.loadNpmTasks('grunt-contrib-jshint');
  // grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task.
  grunt.registerTask('default', ['clean', 'copy', 'concat']);

};
